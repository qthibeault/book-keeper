# Book Keeper

## Purpose
This application manages a collection of books and their associated analysis.


## Architecture
This application has an angularjs front-end and an expressjs backend.

### Front End
The front end has two modes, viewer and creator. In the viewer mode, you can
view a list of books, and when you select one you can view the notes about it.
In the creator mode you can add a new book and analysis about the book. Access
to the creator state is controlled by being logged in. 

### Back End
The backend manges the 
Currently, the datapoints  you can store about a book are:
* Title
* Author
* ISBN
* Genres
* Themes
* Symbols
* Summary
* Important Quotes
These are stored by using a POST request. The back end also supports getting a
list of all books and getting book details.

