import angular from 'angular';
import 'angular-sanitize';

import BooksModule from './books';
import LoginModule from './login';
import RegisterModule from './register';

angular.module('bookLoggr', [
  'ngSanitize',
  LoginModule.name,
  RegisterModule.name,
  BooksModule.name,
])
  .config(['$urlRouterProvider', ($urlRouter) => {
    $urlRouter.otherwise('/');
  }])
  .run(['$log', $log => $log.log('Application online')]);
