import angular from 'angular';
import '@uirouter/angularjs';

import template from './template.html';
import LoginController from './controller';

function registerStates($stateProvider) {
  const loginState = {
    name: 'login',
    url: '/login',
    controller: 'LoginCtrl as ctrl',
    template,
  };

  $stateProvider.state(loginState);
}
registerStates.$inject = ['$stateProvider'];

export default angular.module('bookLoggr.login', ['ui.router'])
  .controller('LoginCtrl', LoginController)
  .config(registerStates);
