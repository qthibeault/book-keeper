export default class Controller {
  constructor($log, $http, $state, $window) {
    this.$log = $log;
    this.$http = $http;
    this.$state = $state;
    this.$window = $window;

    this.showErr = false;

    $log.log('Login controller online');
  }

  login(email, password) {
    this.$http.post('/api/users/authorize', { email, password })
      .then((response) => {
        this.$window.localStorage.setItem('token', response.data.token);
        return this.$state.go('books');
      })
      .catch((err) => {
        this.$log.error(err);
        this.showErr = true;
      });
  }
}
Controller.$inject = ['$log', '$http', '$state', '$window'];
