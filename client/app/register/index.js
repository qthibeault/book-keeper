import angular from 'angular';
import '@uirouter/angularjs';

import template from './template.html';
import RegisterController from './controller';

function registerStates($stateProvider) {
  const registerState = {
    name: 'register',
    url: '/register',
    controller: 'RegisterCtrl as ctrl',
    template,
  };

  $stateProvider.state(registerState);
}
registerStates.$inject = ['$stateProvider'];

export default angular.module('bookLoggr.register', ['ui.router'])
  .controller('RegisterCtrl', RegisterController)
  .config(registerStates);
