function isValidEmail(email) {
  const regexp = /\S+@\S+\.\S+/;
  return regexp.test(email);
}

export default class Controller {
  constructor($log, $http, $state, $window) {
    this.$log = $log;
    this.$http = $http;
    this.$state = $state;
    this.$window = $window;

    this.messages = [];

    $log.log('Register controller online');
  }

  register(email, password) {
    if (!isValidEmail(email)) {
      this.messages.push('Invalid email address');
      return;
    }

    this.$http.post('/api/users', { email, password })
      .then(() => this.$http.post('/api/users/authorize', { email, password }))
      .then((token) => {
        this.$window.localStorage.setItem('token', token);
        this.$state.go('books');
      })
      .catch((err) => {
        this.$log.error(err.data);
        this.messages.push(err.data.msg);
      });
  }
}
Controller.$inject = ['$log', '$http', '$state', '$window'];
