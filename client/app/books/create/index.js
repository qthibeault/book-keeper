import CreateBookState from './book';
import CreateChapterState from './chapter';

const CreateState = {
  name: 'books.create',
  abtract: true,
  url: '/create',
  template: '<ui-view/>'
};

export { CreateBookState, CreateChapterState, CreateState };
