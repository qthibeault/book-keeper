import Template from '../../editor.template.html';
import Controller from './controller';

export default {
  name: 'books.create.book',
  url: '/book',
  controller: Controller,
  controllerAs: 'ctrl',
  template: Template,
  resolve: {
    data: () => ({
      title: '',
      ISBN: '',
      author: '',
      publicationInfo: '',
      summary: '',
      themes: [],
      genres: [],
      symbols: [],
      quotes: [],
    }),
  },
};
