export default class Controller {
  constructor($http, $state, token, data) {
    this.$http = $http;
    this.$state = $state;
    this.data = data;
    this.token = token;

    this.title = 'Create new book';
    this.err = { show: false, message: null };
  }

  submit(book) {
    book.genres = book.genres.map(e => e.value).filter(e => e.length > 0);
    book.themes = book.themes.map(e => e.value).filter(e => e.length > 0);

    console.log('Submitting book', book);

    return this.$http
      .post('/api/books/', book, {
        headers: {
          Authorization: `Bearer ${this.token}`,
        },
      })
      .then(response => this.$state.go('books.view.book', { bookId: response.data._id }, { reload: true }))
      .catch((response) => {
        this.err.show = true;
        this.err.message = response.data.msg;
      });
  }

  cancel() {
    this.$state.go('books');
  }
}
Controller.$inject = ['$http', '$state', 'token', 'data'];
