export default class Controller {
  constructor($http, $state, token, data, bookId) {
    this.$http = $http;
    this.$state = $state;
    this.data = data;
    this.token = token;
    this.bookId = bookId;

    if (!this.bookId) {
      this.$state.go('books');
    }

    this.title = 'Create new chapter';
    this.err = { show: false, message: null };
  }

  submit(chapter) {
    chapter.themes = chapter.themes.map(e => e.value).filter(e => e.length > 0);

    return this.$http
      .post(`/api/books/${this.bookId}/chapters`, chapter, {
        headers: {
          Authorization: `Bearer ${this.token}`,
        },
      })
      .then(() => this.$state.go('books.view.book', { bookId: this.bookId }))
      .catch((response) => {
        this.err.show = true;
        this.err.message = response.data.msg;
      });
  }

  cancel() {
    this.$state.go('books.view.book', { bookId: this.bookId });
  }
}
Controller.$inject = ['$http', '$state', 'token', 'data', 'bookId'];
