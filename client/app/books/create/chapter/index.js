import Template from '../../editor.template.html';
import Controller from './controller';

export default {
  name: 'books.create.chapter',
  url: '/chapter',
  params: {
    bookId: undefined,
  },
  controller: Controller,
  controllerAs: 'ctrl',
  template: Template,
  resolve: {
    bookId: ['$stateParams', $stateParams => $stateParams.bookId],
    data: () => ({
      title: '',
      summary: '',
      themes: [],
      symbols: [],
    }),
  },
};
