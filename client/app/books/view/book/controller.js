export default class Controller {
  constructor(book, chapters, token, $http, $state) {
    this.book = book;
    this.chapters = chapters;
    this.token = token;
    this.$http = $http;
    this.$state = $state;
  }

  deleteBook(bookId) {
    this.$http
      .delete(`/api/books/${bookId}`, {
        headers: {
          Authorization: `Bearer ${this.token}`,
        },
        responseType: 'json',
      })
      .then(() => this.$state.go('books', null, { reload: true }));
  }
}
Controller.$inject = ['book', 'chapters', 'token', '$http', '$state'];
