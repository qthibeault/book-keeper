import Template from './template.html';
import Controller from './controller';

export default {
  name: 'books.view.book',
  url: '/book/:bookId',
  controller: Controller,
  controllerAs: 'ctrl',
  template: Template,
  resolve: {
    id: ['$stateParams', $stateParams => $stateParams.bookId],
    book: ['$http', 'token', 'id', ($http, token, id) => $http.get(`/api/books/${id}`, {
      headers: {
        Authorization: `Bearer ${token}`,
      },
      responseType: 'json',
    }).then(response => response.data)],
    chapters: ['$http', 'token', 'id', ($http, token, id) => $http.get(`/api/books/${id}/chapters`, {
      headers: {
        Authorization: `Bearer ${token}`,
      },
      responseType: 'json',
    }).then(response => response.data)],
  },
};
