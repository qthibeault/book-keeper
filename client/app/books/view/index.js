import ViewBookState from './book';

const ViewState = {
  name: 'books.view',
  abstract: true,
  url: '/view',
  template: '<ui-view/>'
};

export { ViewState, ViewBookState };
