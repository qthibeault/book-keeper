import Template from '../../editor.template.html';
import Controller from './controller';

export default {
  name: 'books.edit.book',
  url: '/book/:bookId',
  controller: Controller,
  controllerAs: 'ctrl',
  template: Template,
  resolve: {
    bookId: ['$stateParams', $stateParams => $stateParams.bookId],
    data: ['$http', 'token', 'bookId', ($http, token, id) => $http.get(`/api/books/${id}`, {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    }).then(response => response.data)],
  },
};
