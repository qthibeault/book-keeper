export default class Controller {
  constructor($http, $state, token, data, bookId) {
    this.$http = $http;
    this.$state = $state;
    this.token = token;
    this.data = data;
    this.bookId = bookId;

    this.title = `Edit book ${data.title}`;
    this.err = { show: false, message: null };
  }

  submit(book) {
    this.$http
      .put(`/api/books/${this.bookId}`, book, {
        headers: {
          Authorization: `Bearer ${this.token}`,
        },
      })
      .then(() => {
        this.$state.go('books.view.book', { bookId: this.bookId });
      })
      .catch((response) => {
        this.err.show = true;
        this.err.message = response.data.msg;
      });
  }

  cancel() {
    this.$state.go('books.view.book', { bookId: this.bookId });
  }
}
Controller.$inject = ['$http', '$state', 'token', 'data', 'bookId'];
