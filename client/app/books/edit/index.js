import EditBookState from './book';
import EditChapterState from './chapter';

const EditState = {
  name: 'books.edit',
  url: '/edit',
  abstract: true,
  template: '<ui-view/>',
};

export { EditState, EditBookState, EditChapterState };
