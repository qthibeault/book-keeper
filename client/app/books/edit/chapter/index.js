import Template from '../../editor.template.html';
import Controller from './controller';

export default {
  name: 'books.edit.chapter',
  params: {
    bookId: undefined,
  },
  url: '/chapter/:chapterId',
  controller: Controller,
  controllerAs: 'ctrl',
  template: Template,
  resolve: {
    bookId: ['$stateParams', $stateParams => $stateParams.bookId],
    chapterId: ['$stateParams', $stateParams => $stateParams.chapterId],
    data: ['$http', 'token', 'bookId', 'chapterId', ($http, token, bookId, chapterId) => $http.get(`/api/books/${bookId}/chapters/${chapterId}`, {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    })],
  },
};
