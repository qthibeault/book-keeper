export default class Controller {
  constructor($http, $state, token, data, bookId, chapterId) {
    this.$http = $http;
    this.$state = $state;
    this.token = token;
    this.data = data;
    this.bookId = bookId;
    this.chapterId = chapterId;

    this.title = `Edit book ${data.title}`;
    this.err = { show: false, message: null };
  }

  submit(chapter) {
    this.$http
      .put(`/api/books/${this.bookId}/chapters/${this.chapterId}`, chapter, {
        headers: {
          Authorization: `Bearer ${this.token}`,
        },
      })
      .then(() => {
        this.$state.go('books.view.book', { bookId: this.bookId });
      })
      .catch((response) => {
        this.err.show = true;
        this.err.message = response.data.msg;
      });
  }
}
Controller.$inject = [];
