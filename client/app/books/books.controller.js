export default class Controller {
  constructor($log, books) {
    this.$log = $log;
    this.books = books;
  }
}
Controller.$inject = ['$log', 'books'];
