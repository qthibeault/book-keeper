import angular from 'angular';
import '@uirouter/angularjs';

import Controller from './books.controller';
import Template from './books.template.html';

import { CreateState, CreateBookState, CreateChapterState } from './create';
import { EditState, EditBookState, EditChapterState } from './edit';
import { ViewState, ViewBookState } from './view';

function registerStates($stateProvider) {
  const BooksState = {
    name: 'books',
    url: '',
    controller: Controller,
    controllerAs: 'ctrl',
    template: Template,
    resolve: {
      token: ['$window', '$state', '$http', ($window, $state, $http) => {
        const token = $window.localStorage.getItem('token');

        return $http.post('/api/users/renew', { token })
          .then(response => response.data.token)
          .catch(() => $state.go('login'));
      }],
      books: ['$http', 'token', ($http, token) => $http.get('/api/books', {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      }).then(response => response.data)],
    },
  };

  $stateProvider.state(BooksState);

  $stateProvider.state(CreateState);
  $stateProvider.state(CreateBookState);
  $stateProvider.state(CreateChapterState);

  $stateProvider.state(EditState);
  $stateProvider.state(EditBookState);
  $stateProvider.state(EditChapterState);

  $stateProvider.state(ViewState);
  $stateProvider.state(ViewBookState);
}
registerStates.$inject = ['$stateProvider'];

export default angular.module('bookLoggr.books', ['ui.router'])
  .config(registerStates);
