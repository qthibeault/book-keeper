const gulp = require('gulp');
const mocha = require('gulp-mocha');
const babel = require('gulp-babel');
const eslint = require('gulp-eslint');
const nodemon = require('gulp-nodemon');
const sourcemaps = require('gulp-sourcemaps');
const rename = require('gulp-rename');
const browserify = require('gulp-browserify');
const html = require('browserify-html');

const htmlOptions = {
  exts: ['html'],
  minify: {
    collapseWhitespace: true,
    collapseBooleanAttributes: true,
    removeRedundantAttributes: true,
    removeEmptyAttributes: true,
  },
};

gulp.task('default', ['lint', 'build']);
gulp.task('build', ['build:server', 'build:client']);

gulp.task('watch:client', ['build:client'], () => gulp.watch([
  'app/**/*.js',
  'app/**/*.html',
], [
  'build:client'
]));

gulp.task('build:client', () => gulp.src('client/app/app.js')
  .pipe(browserify({
    insertGlobals: true,
    transform: ['babelify', [html, htmlOptions]],
  }))
  .pipe(rename('bundle.js'))
  .pipe(gulp.dest('client/_dist')));

gulp.task('build:server', () => gulp.src('lib/**/*.js')
  .pipe(sourcemaps.init())
  .pipe(babel())
  .pipe(sourcemaps.write('.'))
  .pipe(gulp.dest('_dist')));

gulp.task('lint', () => gulp.src(['lib/**/*.js', 'client/app/**/*.js'])
  .pipe(eslint())
  .pipe(eslint.format())
  .pipe(eslint.failAfterError()));

gulp.task('serve', ['watch:client'], () => nodemon({
  script: '_dist/index.js',
  tasks: [
    'build:server',
    'build:client',
  ],
  watch: [ 'lib/**/*.js' ],
}));

gulp.task('test', ['build:server'], () => gulp.src('test/**/*.js')
  .pipe(mocha({ exit: true, timeout: 5000 })));
