/* global describe it before beforeEach */
process.env.NODE_ENV = 'test';

const jwt = require('jsonwebtoken');
const chai = require('chai');
const chaiHttp = require('chai-http');
const mongoose = require('mongoose');
const config = require('config');

const Book = require('../_dist/models/book').default;
const server = require('../_dist/server').default;

chai.should();
chai.use(chaiHttp);

describe('Book tests', () => {
  let token = null;
  let bookId = null;

  before((done) => {
    token = jwt.sign({ user: 'example@gmail.com' }, config.get('secret'));
    done();
  });

  beforeEach((done) => {
    Book.remove({})
      .then(() => {
        const book = new Book({
          owner: 'example@gmail.com',
          title: 'Over the air and under my pants',
          author: 'P. Smellington',
          publicationInfo: 'Mid-23rd century',
          genres: ['Horror', 'Romance'],
          themes: ['Mortality', 'Censorship'],
          symbols: [{
            name: 'Toilet Plunger',
            description: 'A physical metaphor for the power of love',
          }],
        });

        return book.save();
      })
      .then((book) => {
        bookId = book.id;
        token = jwt.sign({ user: 'example@gmail.com' }, config.get('secret'));
      })
      .then(() => done());
  });

  describe('Retrieval', () => {
    it('Should not get all books with no token', (done) => {
      chai.request(server)
        .get('/api/books')
        .end((err, res) => {
          res.should.have.status(401);
          done();
        });
    });

    it('Should not get all books with bad token', (done) => {
      const badToken = jwt.sign({ user: 'example@gmail.com' }, 'incorrect secret');

      chai.request(server)
        .get('/api/books')
        .set('Authorization', `Bearer ${badToken}`)
        .end((err, res) => {
          res.should.have.status(403);
          done();
        });
    });

    it('Should get all books for user', (done) => {
      chai.request(server)
        .get('/api/books')
        .set('Authorization', `Bearer ${token}`)
        .end((err, res) => {
          res.should.have.status(200);
          res.body.should.be.a('array');
          res.body.length.should.eql(1);
          done();
        });
    });

    it('Should get a single book for user', (done) => {
      chai.request(server)
        .get(`/api/books/${bookId}`)
        .set('Authorization', `Bearer ${token}`)
        .end((err, res) => {
          res.should.have.status(200);
          res.body.should.be.a('object');
          res.body.should.have.property('title');
          done();
        });
    });

    it('Should not get a book for user with bad ID', (done) => {
      chai.request(server)
        .get('/api/books/1')
        .set('Authorization', `Bearer ${token}`)
        .end((err, res) => {
          res.should.have.status(400);
          done();
        });
    });

    it('Should not get a missing book for user', (done) => {
      chai.request(server)
        .get(`/api/books/${new mongoose.Types.ObjectId(1).toHexString()}`)
        .set('Authorization', `Bearer ${token}`)
        .end((err, res) => {
          res.should.have.status(404);
          done();
        });
    });
  });

  describe('Creation', () => {
    it('Should create book for a user', (done) => {
      chai.request(server)
        .post('/api/books')
        .set('Authorization', `Bearer ${token}`)
        .send({
          title: 'Example',
        })
        .end((err, res) => {
          res.should.have.status(201);
          done();
        });
    });

    it('Should not create a book without title field', (done) => {
      chai.request(server)
        .post('/api/books')
        .set('Authorization', `Bearer ${token}`)
        .send({
          author: 'Gary McMustashe',
        })
        .end((err, res) => {
          res.should.have.status(400);
          done();
        });
    });
  });

  describe('Modification', () => {
    it('Should update book for a user', (done) => {
      chai.request(server)
        .put(`/api/books/${bookId}`)
        .set('Authorization', `Bearer ${token}`)
        .send({
          title: 'Example',
        })
        .end((err, res) => {
          res.should.have.status(200);
          done();
        });
    });

    it('Should not be able to modify an unowned book', (done) => {
      const otherToken = jwt.sign({ user: 'test@yahoo.com' }, 'secret');

      chai.request(server)
        .put(`/api/books/${bookId}`)
        .set('Authorization', `Bearer ${otherToken}`)
        .send({
          title: 'Example',
        })
        .end((err, res) => {
          res.should.have.status(403);
          done();
        });
    });

    it('Should not be able to modify a missing book', (done) => {
      chai.request(server)
        .put(`/api/books/${new mongoose.Types.ObjectId(1).toHexString()}`)
        .set('Authorization', `Bearer ${token}`)
        .send({
          title: 'Example',
        })
        .end((err, res) => {
          res.should.have.status(404);
          done();
        });
    });
  });

  describe('Removal', () => {
    it('Should remove a book for a user', (done) => {
      chai.request(server)
        .delete(`/api/books/${bookId}`)
        .set('Authorization', `Bearer ${token}`)
        .end((err, res) => {
          res.should.have.status(200);
          done();
        });
    });

    it('Should not remove a book for a different user', (done) => {
      const otherToken = jwt.sign({ user: 'test@yahoo.com' }, 'secret');

      chai.request(server)
        .delete(`/api/books/${bookId}`)
        .set('Authorization', `Bearer ${otherToken}`)
        .end((err, res) => {
          res.should.have.status(403);
          done();
        });
    });

    it('Should not remove a missing book', (done) => {
      chai.request(server)
        .delete(`/api/books/${new mongoose.Types.ObjectId(1).toHexString()}`)
        .set('Authorization', `Bearer ${token}`)
        .end((err, res) => {
          res.should.have.status(404);
          done();
        });
    });
  });
});
