/* global describe it before after */
process.env.NODE_ENV = 'test';

const config = require('config');
const jwt = require('jsonwebtoken');
const chai = require('chai');
const chaiHttp = require('chai-http');

const User = require('../_dist/models/user').default;
const server = require('../_dist/server').default;

chai.should();
chai.use(chaiHttp);

describe('User tests', () => {
  before((done) => {
    User.remove({}).then(() => done());
  });

  describe('Registration', () => {
    it('Should create new user', (done) => {
      const user = {
        email: 'example@gmail.com',
        password: 'test1234',
      };

      chai.request(server)
        .post('/api/users')
        .send(user)
        .end((err, res) => {
          res.should.have.status(201);
          done();
        });
    });

    it('Should not create a user', (done) => {
      const user = {
        email: 'example2@gmail.com',
      };

      chai.request(server)
        .post('/api/users')
        .send(user)
        .end((err, res) => {
          res.should.have.status(400);
          done();
        });
    });

    it('Should not create duplicate user', (done) => {
      const user = {
        email: 'example@gmail.com',
        password: 'something secret',
      };

      chai.request(server)
        .post('/api/users')
        .send(user)
        .end((err, res) => {
          res.should.have.status(409);
          done();
        });
    });
  });

  describe('Authentication', () => {
    it('Should authenticate existing user', (done) => {
      const credentials = {
        email: 'example@gmail.com',
        password: 'test1234',
      };

      chai.request(server)
        .post('/api/users/authorize')
        .send(credentials)
        .end((err, res) => {
          res.should.have.status(200);
          res.body.should.have.property('token');
          done();
        });
    });

    it('Should not authenticate with bad credentials', (done) => {
      const credentials = {
        email: 'example@gmail.com',
        password: 'wrong password',
      };

      chai.request(server)
        .post('/api/users/authorize')
        .send(credentials)
        .end((err, res) => {
          res.should.have.status(403);
          done();
        });
    });

    it('Should not authenticate non-existant user', (done) => {
      const credentials = {
        email: 'example2@gmail.com',
        password: 'secret',
      };

      chai.request(server)
        .post('/api/users/authorize')
        .send(credentials)
        .end((err, res) => {
          res.should.have.status(403);
          done();
        });
    });
  });

  describe('Renewal', () => {
    it('should return a new token', (done) => {
      const secret = config.get('secret');
      const token = jwt.sign({
        user: 'example@gmail.com',
      }, secret, {
        expiresIn: '2h',
      });

      chai.request(server)
        .post('/api/users/renew')
        .send({ token })
        .end((err, res) => {
          res.should.have.status(200);
          res.body.should.have.property('token');
          res.body.token.should.not.equal(token);
          done();
        });
    });

    it('Should not return a new token without a previous token', (done) => {
      chai.request(server)
        .post('/api/users/renew')
        .send({})
        .end((err, res) => {
          res.should.have.status(400);
          done();
        });
    });

    it('Should not return a new token with a invalid token', (done) => {
      const token = jwt.sign({
        user: 'example@gmail.com',
      }, 'bad secret');

      chai.request(server)
        .post('/api/users/renew')
        .send({ token })
        .end((req, res) => {
          res.should.have.status(403);
          done();
        });
    });
  });

  after((done) => {
    User.remove({}).then(() => done());
  });
});
