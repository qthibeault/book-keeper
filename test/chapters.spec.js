/* global describe it before beforeEach */
process.env.NODE_ENV = 'test';

const jwt = require('jsonwebtoken');
const chai = require('chai');
const chaiHttp = require('chai-http');
const mongoose = require('mongoose');
const config = require('config');

const Book = require('../_dist/models/book').default;
const Chapter = require('../_dist/models/chapter').default;
const server = require('../_dist/server').default;

chai.should();
chai.use(chaiHttp);

describe('Chapter tests', () => {
  let token = null;
  let bookId = null;
  let chapterId = null;

  const book = new Book({
    owner: 'example@gmail.com',
    title: 'Over the air and under my pants',
    author: 'P. Smellington',
    publicationInfo: 'Mid-23rd century',
    genres: ['Horror', 'Romance'],
    themes: ['Mortality', 'Censorship'],
    symbols: [{
      name: 'Toilet Plunger',
      description: 'A physical metaphor for the power of love',
    }],
  });

  before((done) => {
    token = jwt.sign({ user: 'example@gmail.com' }, config.get('secret'));
    Book.remove({})
      .then(() => book.save())
      .then((record) => {
        bookId = record._id;
        done();
      });
  });

  beforeEach((done) => {
    Chapter.remove({})
      .then(() => {
        const chapter = new Chapter({
          parent: bookId,
          title: 'The Ages of the Bologna',
          summary: 'The world ends, and a scourge of mayonaise is unleashed upon the earth.',
          themes: [],
          symbols: [],
        });
        return chapter.save();
      })
      .then((chapter) => {
        chapterId = chapter._id;
        done();
      });
  });

  describe('Loading chapters for a book', () => {
    it('Should get all chapters', (done) => {
      chai.request(server)
        .get(`/api/books/${bookId}/chapters`)
        .set('Authorization', `Bearer ${token}`)
        .end((err, res) => {
          res.should.have.status(200);
          res.body.should.be.a('array');
          res.body.should.have.length(1);
          done();
        });
    });

    it('Should get one chapter', (done) => {
      chai.request(server)
        .get(`/api/books/${bookId}/chapters/${chapterId}`)
        .set('Authorization', `Bearer ${token}`)
        .end((err, res) => {
          res.should.have.status(200);
          res.body.should.have.property('title');
          done();
        });
    });

    it('Should not get a chapter without a token', (done) => {
      chai.request(server)
        .get(`/api/books/${bookId}/chapters/${chapterId}`)
        .end((err, res) => {
          res.should.have.status(401);
          done();
        });
    });

    it('Should not get a chapter for a book that is not owned', (done) => {
      const otherToken = jwt.sign({ user: 'example@yahoo.com' }, config.get('secret'));

      chai.request(server)
        .get(`/api/books/${bookId}/chapters/${chapterId}`)
        .set('Authorization', `Bearer ${otherToken}`)
        .end((err, res) => {
          res.should.have.status(403);
          done();
        });
    });

    it('Should not get a chapter for a book that does not exist', (done) => {
      chai.request(server)
        .get(`/api/books/${new mongoose.Types.ObjectId(1).toHexString()}/chapters/${chapterId}`)
        .set('Authorization', `Bearer ${token}`)
        .end((err, res) => {
          res.should.have.status(404);
          done();
        });
    });

    it('Should not get a chapter that does not exist', (done) => {
      chai.request(server)
        .get(`/api/books/${bookId}/chapters/${new mongoose.Types.ObjectId(1).toHexString()}`)
        .set('Authorization', `Bearer ${token}`)
        .end((err, res) => {
          res.should.have.status(404);
          done();
        });
    });

    it('Should not get chapters for a book that is not owned', (done) => {
      const otherToken = jwt.sign({ user: 'example@yahoo.com' }, config.get('secret'));

      chai.request(server)
        .get(`/api/books/${bookId}/chapters`)
        .set('Authorization', `Bearer ${otherToken}`)
        .end((err, res) => {
          res.should.have.status(403);
          done();
        });
    });

    it('Should not get chapters for a book that does not exist', (done) => {
      chai.request(server)
        .get(`/api/books/${new mongoose.Types.ObjectId(1).toHexString()}/chapters`)
        .set('Authorization', `Bearer ${token}`)
        .end((err, res) => {
          res.should.have.status(404);
          done();
        });
    });
  });

  describe('Creating a chapter for a book', () => {
    it('Should create a chapter', (done) => {
      chai.request(server)
        .post(`/api/books/${bookId}/chapters`)
        .send({ title: 'Of cooking and eating dwarf' })
        .set('Authorization', `Bearer ${token}`)
        .end((err, res) => {
          res.should.have.status(201);
          done();
        });
    });

    it('Should not create a chapter without a title', (done) => {
      chai.request(server)
        .post(`/api/books/${bookId}/chapters`)
        .send({ })
        .set('Authorization', `Bearer ${token}`)
        .end((err, res) => {
          res.should.have.status(400);
          done();
        });
    });

    it('Should not create a chapter for a missing book', (done) => {
      chai.request(server)
        .post(`/api/books/${new mongoose.Types.ObjectId(1).toHexString()}/chapters`)
        .send({ title: 'Part 1: The coming of the ketchup warrior' })
        .set('Authorization', `Bearer ${token}`)
        .end((err, res) => {
          res.should.have.status(404);
          done();
        });
    });

    it('Should not create a chapter for a book with a different owner', (done) => {
      const differentToken = jwt.sign({ user: 'example@yahoo.com' }, config.get('secret'));

      chai.request(server)
        .post(`/api/books/${bookId}/chapters`)
        .send({ title: 'The planning of the voyage' })
        .set('Authorization', `Bearer ${differentToken}`)
        .end((err, res) => {
          res.should.have.status(403);
          done();
        });
    });
  });

  describe('Updating a chapter for a book', () => {
    it('Should update a chapter', (done) => {
      chai.request(server)
        .put(`/api/books/${bookId}/chapters/${chapterId}`)
        .send({ title: 'Creating a monster-pus' })
        .set('Authorization', `Bearer ${token}`)
        .end((err, res) => {
          res.should.have.status(200);

          Chapter.findById(chapterId)
            .then((chapter) => {
              chapter.should.have.property('title', 'Creating a monster-pus');
              done();
            });
        });
    });

    it('Should not update a missing chapter', (done) => {
      chai.request(server)
        .put(`/api/books/${bookId}/chapters/${new mongoose.Types.ObjectId(1).toHexString()}`)
        .send({ title: 'Can opening for worms' })
        .set('Authorization', `Bearer ${token}`)
        .end((err, res) => {
          res.should.have.status(404);
          done();
        });
    });

    it('Should not update a chapter for a missing book', (done) => {
      chai.request(server)
        .put(`/api/books/${new mongoose.Types.ObjectId(1).toHexString()}/chapters/${chapterId}`)
        .send({ title: 'Who can I upset today: A Troll\'s guide to the internet' })
        .set('Authorization', `Beader ${token}`)
        .end((err, res) => {
          res.should.have.status(404);
          done();
        });
    });

    it('Should not update a book that is not owned', (done) => {
      const differentToken = jwt.sign({ user: 'example@yahoo.com' }, config.get('secret'));

      chai.request(server)
        .put(`/api/books/${bookId}/chapters/${chapterId}`)
        .send({ title: 'Chapter 1: Who poo in loo' })
        .set('Authorization', `Bearer ${differentToken}`)
        .end((err, res) => {
          res.should.have.status(403);
          done();
        });
    });
  });

  describe('Deleting a chapter for a book', () => {
    it('Should not delete a chapter of a missing book', (done) => {
      chai.request(server)
        .delete(`/api/books/${new mongoose.Types.ObjectId(1).toHexString()}/chapters/${chapterId}`)
        .set('Authorization', `Bearer ${token}`)
        .end((err, res) => {
          res.should.have.status(404);
          done();
        });
    });

    it('Should not delete a missing chapter', (done) => {
      chai.request(server)
        .delete(`/api/books/${bookId}/chapters/${new mongoose.Types.ObjectId(1).toHexString()}`)
        .set('Authorization', `Bearer ${token}`)
        .end((err, res) => {
          res.should.have.status(404);
          done();
        });
    });

    it('Should not delete a chapter that is not owned', (done) => {
      const otherToken = jwt.sign({ user: 'example@yahoo.com' }, config.get('secret'));

      chai.request(server)
        .delete(`/api/books/${bookId}/chapters/${chapterId}`)
        .set('Authorization', `Bearer ${otherToken}`)
        .end((err, res) => {
          res.should.have.status(403);
          done();
        });
    });

    it('Should delete a chapter', (done) => {
      chai.request(server)
        .delete(`/api/books/${bookId}/chapters/${chapterId}`)
        .set('Authorization', `Bearer ${token}`)
        .end((err, res) => {
          res.should.have.status(200);
          done();
        });
    });
  });
});
