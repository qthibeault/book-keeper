/* global describe it */

const chai = require('chai');
const chaiHttp = require('chai-http');

const server = require('../_dist/server.js').default;

chai.use(chaiHttp);

describe('App tests', () => {
  it('Should return the index page', (done) => {
    chai.request(server)
      .get('/app')
      .end((err, res) => {
        res.should.have.status(200);
        done();
      });
  });

  it('Should return the app bundle', (done) => {
    chai.request(server)
      .get('/app/bundle.js')
      .end((err, res) => {
        res.should.have.status(200);
        done();
      });
  });
});
