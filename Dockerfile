FROM node:latest

WORKDIR /opt/webapp
COPY package.json package-lock.json ./
RUN npm install

COPY ./_dist ./_dist
COPY ./config ./config
COPY ./client/_dist ./client/_dist
COPY ./client/app/index.html ./client/app/

CMD ["node", "_dist/index.js"]
