import http from 'http';
import config from 'config';

import logger from './logger';
import server from './server';

(function main() {
  const port = config.get('port');

  http
    .createServer(server)
    .listen(port, () => {
      logger.info(`API running on localhost:${port}`);
    });
}());
