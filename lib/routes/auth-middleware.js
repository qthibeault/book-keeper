import config from 'config';
import jwt from 'jsonwebtoken';

function authMiddleware(req, res, next) {
  const secret = config.get('secret');
  const authHeader = req.get('Authorization');

  if (!authHeader) {
    return res.status(401).json({
      statusCode: 401,
      msg: 'Missing Authorization header',
    });
  }

  const authToken = authHeader.substr(7);

  return jwt.verify(authToken, secret, (err, decoded) => {
    if (err) {
      return res.status(403).json({
        statusCode: 403,
        msg: 'Invalid token',
      });
    }

    req.user = decoded.user;
    return next();
  });
}

export default authMiddleware;
