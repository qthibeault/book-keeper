import path from 'path';
import { Router } from 'express';

const router = new Router();

/* serve index page */
router.get('/', (req, res) => res.sendFile('index.html', {
  root: path.join(__dirname, '..', '..', 'client', 'app'),
}));

router.get('/bundle.js', (req, res) => res.sendFile('bundle.js', {
  root: path.join(__dirname, '..', '..', 'client', '_dist'),
}));

router.use((req, res) => res.status(404)
  .json({ statusCode: 404, msg: 'Not Found' }));

export default router;
