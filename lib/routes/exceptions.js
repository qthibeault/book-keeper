class WebException {
  constructor(statusCode, message) {
    this.name = 'WebException';
    this.statusCode = statusCode;
    this.message = message;
  }
}

export default WebException;
