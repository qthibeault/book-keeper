import jwt from 'jsonwebtoken';
import { Router } from 'express';
import config from 'config';
import bcrypt from 'bcrypt';

import User from '../models/user';
import WebException from './exceptions';

const router = new Router();

router.post('/', (req, res) => {
  const user = new User(req.body);

  user.validate()
    .catch((err) => {
      throw new WebException(400, err.message);
    })
    .then(() => User.findOne({
      email: user.email,
    }))
    .then((record) => {
      if (record) {
        throw new WebException(409, `User with email ${user.email} already exists`);
      }

      return bcrypt.hash(user.password, config.get('salt'));
    })
    .then((hash) => {
      user.password = hash;
      return user.save();
    })
    .then(() => res.status(201).end())
    .catch((err) => {
      const response = {
        msg: err.message,
        statusCode: err.statusCode || 500,
      };

      return res.status(err.statusCode).json(response);
    });
});

router.post('/authorize', (req, res) => {
  const secret = config.get('secret');
  const user = new User(req.body);

  user.validate()
    .catch((err) => {
      throw new WebException(400, err.message);
    })
    .then(() => User.findOne({
      email: user.email,
    }))
    .then((record) => {
      if (!record) {
        throw new WebException(403, 'Invalid email or password');
      }

      return bcrypt.compare(user.password, record.password);
    })
    .then((success) => {
      if (!success) {
        throw new WebException(403, 'Invalid email or password');
      }

      return jwt.sign({
        user: user.email,
      }, secret, {
        expiresIn: '1h',
      });
    })
    .then(token => res.status(200).json({ token }))
    .catch((err) => {
      const response = {
        msg: err.message,
        statusCode: err.statusCode || 500,
      };

      return res.status(response.statusCode).json(response);
    });
});

router.post('/renew', (req, res) => {
  const { token } = req.body;
  const secret = config.get('secret');

  if (!token) {
    return res.status(400).json({ statusCode: 400, msg: 'Missing token field' });
  }

  try {
    const decoded = jwt.verify(token, secret);
    const newToken = jwt.sign({
      user: decoded.user,
    }, secret, {
      expiresIn: '1h',
    });

    return res.status(200).json({ token: newToken });
  } catch (err) {
    return res.status(403).json({ statusCode: 403, msg: 'Cannot refresh invalid token' });
  }
});

export default router;
