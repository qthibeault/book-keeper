import { Router } from 'express';
import mongoose from 'mongoose';

import Book from '../models/book';
import WebException from './exceptions';
import authMiddleware from './auth-middleware';
import chaptersRoutes from './chapters';

const router = new Router();
router.use(chaptersRoutes);
router.use(authMiddleware);

router.get('/', (req, res) => {
  Book.find({ owner: req.user })
    .then(books => res.json(books))
    .catch(err => res.status(500).json({ statusCode: 500, msg: err.message }));
});

router.post('/', (req, res) => {
  const book = new Book(req.body);

  book.validate()
    .then(() => {
      book.owner = req.user;
      return book.save()
        .then(newBook => res.status(201).json(newBook))
        .catch(err => res.status(500).json({ statusCode: 500, msg: err.message }));
    })
    .catch(err => res.status(400).json({ statusCode: 500, msg: err.message }));
});

router.get('/:bookId', (req, res) => {
  if (!mongoose.Types.ObjectId.isValid(req.params.bookId)) {
    return res.status(400).json({ statusCode: 400, msg: 'Invalid book ID' });
  }

  return Book.findById(req.params.bookId)
    .then((book) => {
      if (!book) throw new WebException(404, `Book with id: ${req.params.bookId} not found`);
      if (book.owner !== req.user) throw new WebException(403, 'Not authorized to access resource');

      return res.status(200)
        .json(book);
    })
    .catch((err) => {
      const response = {
        msg: err.message,
      };

      if (err instanceof WebException) {
        response.statusCode = err.statusCode;
      } else {
        response.statusCode = 500;
      }

      res.status(response.statusCode).json(response);
    });
});

router.put('/:bookId', (req, res) => {
  if (!mongoose.Types.ObjectId.isValid(req.params.bookId)) {
    return res.status(400).json({ statusCode: 400, msg: 'Invalid book ID' });
  }

  return Book.findById(req.params.bookId)
    .then((book) => {
      if (!book) throw new WebException(404, `Book with id: ${req.params.bookId} not found`);
      if (book.owner !== req.user) throw new WebException(403, 'Not authorized to access resource');

      return Book.findByIdAndUpdate(req.params.bookId, req.body);
    })
    .then(() => res.status(200).end())
    .catch((err) => {
      const response = {
        msg: err.message,
      };

      if (err instanceof WebException) {
        response.statusCode = err.statusCode;
      } else {
        response.statusCode = 500;
      }

      res.status(response.statusCode).json(response);
    });
});

router.delete('/:bookId', (req, res) => {
  if (!mongoose.Types.ObjectId.isValid(req.params.bookId)) {
    return res.status(400).json({ statusCode: 400, msg: 'Invalid book ID' });
  }

  return Book.findById(req.params.bookId)
    .then((book) => {
      if (!book) throw new WebException(404, `Book with id: ${req.params.bookId} not found`);
      if (book.owner !== req.user) throw new WebException(403, 'Not authorized to access resource');

      return Book.findByIdAndRemove(req.params.bookId);
    })
    .then(() => res.status(200).end())
    .catch((err) => {
      const response = {
        msg: err.message,
      };

      if (err instanceof WebException) {
        response.statusCode = err.statusCode;
      } else {
        response.statusCode = 500;
      }

      res.status(response.statusCode).json(response);
    });
});

export default router;
