import { Router } from 'express';
import mongoose from 'mongoose';

import Book from '../models/book';
import Chapter from '../models/chapter';
import WebException from './exceptions';
import authMiddleware from './auth-middleware';

const router = new Router();
router.use(authMiddleware);

router.get('/:bookId/chapters', (req, res) => {
  if (!mongoose.Types.ObjectId.isValid(req.params.bookId)) {
    return res.status(400).json({
      statusCode: 400,
      msg: 'Invalid book ID',
    });
  }

  return Book.findById(req.params.bookId)
    .then((book) => {
      if (!book) {
        throw new WebException(404, `Book with id: ${req.params.bookId} not found`);
      } else if (book.owner !== req.user) {
        throw new WebException(403, 'Not authorized to access resource');
      }

      return Chapter.find({
        parent: req.params.bookId,
      });
    })
    .then(chapters => res.status(200).json(chapters))
    .catch((err) => {
      const response = {
        msg: err.message,
        statusCode: err.statusCode || 500,
      };

      res.status(response.statusCode).json(response);
    });
});

router.post('/:bookId/chapters', (req, res) => {
  if (!mongoose.Types.ObjectId.isValid(req.params.bookId)) {
    return res.status(400).json({
      statusCode: 400,
      msg: 'Invalid book ID',
    });
  }

  const chapter = new Chapter(req.body);
  return Book.findById(req.params.bookId)
    .then((book) => {
      if (!book) {
        throw new WebException(404, `Book with id: ${req.params.bookId} does not exist`);
      }
      return book.owner === req.user;
    })
    .catch((err) => {
      if (err instanceof WebException) {
        throw err;
      }
      throw new WebException(500, err.message);
    })
    .then((owned) => {
      if (!owned) {
        throw new WebException(403, 'Do not have permission to access this resource');
      }
      return chapter.validate();
    })
    .catch((err) => {
      if (err instanceof WebException) {
        throw err;
      }
      throw new WebException(400, err.message);
    })
    .then(() => {
      chapter.parent = req.params.bookId;
      return chapter.save();
    })
    .then(() => res.status(201).end())
    .catch((err) => {
      const response = {
        msg: err.message,
        statusCode: err.statusCode || 500,
      };

      return res.status(response.statusCode).json(response);
    });
});

router.get('/:bookId/chapters/:chapterId', (req, res) => {
  if (!mongoose.Types.ObjectId.isValid(req.params.bookId)) {
    return res.status(400).json({
      statusCode: 400,
      msg: 'Invalid book ID',
    });
  }

  if (!mongoose.Types.ObjectId.isValid(req.params.chapterId)) {
    return res.status(400).json({
      statusCode: 400,
      msg: 'Invalid chapter ID',
    });
  }

  return Book.findById(req.params.bookId)
    .then((book) => {
      if (!book) {
        throw new WebException(404, `Book with id: ${req.params.bookId} not found`);
      } else if (book.owner !== req.user) {
        throw new WebException(403, 'Not authorized to access resource');
      }

      return Chapter.findById(req.params.chapterId);
    })
    .then((chapter) => {
      if (!chapter) {
        throw new WebException(404, `Chapter with id: ${req.params.chapterId} not found`);
      }

      return res.status(200).json(chapter);
    })
    .catch((err) => {
      const response = {
        msg: err.message,
        statusCode: err.statusCode || 500,
      };

      res.status(response.statusCode).json(response);
    });
});

router.put('/:bookId/chapters/:chapterId', (req, res) => {
  if (!mongoose.Types.ObjectId.isValid(req.params.bookId)) {
    return res.status(400).json({
      statusCode: 400,
      msg: 'Invalid book ID',
    });
  }

  if (!mongoose.Types.ObjectId.isValid(req.params.chapterId)) {
    return res.status(400).json({
      statusCode: 400,
      msg: 'Invalid chapter ID',
    });
  }

  return Book.findById(req.params.bookId)
    .then((book) => {
      if (!book) {
        throw new WebException(404, `Book with id: ${req.params.bookId} not found`);
      }

      return book.owner === req.user;
    })
    .then((owned) => {
      if (!owned) {
        throw new WebException(403, 'Not authorized to access resource');
      }

      return Chapter.findById(req.params.chapterId);
    })
    .then((chapter) => {
      if (!chapter) {
        throw new WebException(404, `Chapter with id: ${req.params.chapterId} not found`);
      }

      return Chapter.findByIdAndUpdate(req.params.chapterId, req.body);
    })
    .then(() => res.status(200).end())
    .catch((err) => {
      const response = {
        msg: err.message,
        statusCode: err.statusCode || 500,
      };

      return res.status(response.statusCode).json(response);
    });
});

router.delete('/:bookId/chapters/:chapterId', (req, res) => {
  if (!mongoose.Types.ObjectId.isValid(req.params.bookId)) {
    return res.status(400).json({
      statusCode: 400,
      msg: 'Invalid book ID',
    });
  }

  if (!mongoose.Types.ObjectId.isValid(req.params.chapterId)) {
    return res.status(400).json({
      statusCode: 400,
      msg: 'Invalid chapter ID',
    });
  }

  return Book.findById(req.params.bookId)
    .then((book) => {
      if (!book) {
        throw new WebException(404, `Book with id: ${req.params.bookId} not found`);
      }

      return book.owner === req.user;
    })
    .then((owned) => {
      if (!owned) {
        throw new WebException(403, 'Not authorized to access resource');
      }

      return Chapter.findById(req.params.chapterId);
    })
    .then((chapter) => {
      if (!chapter) {
        throw new WebException(404, `Chapter with id: ${req.params.chapterId} not found`);
      }

      return Chapter.findByIdAndRemove(req.params.chapterId);
    })
    .then(() => res.status(200).end())
    .catch((err) => {
      const response = {
        msg: err.message,
        statusCode: err.statusCode || 500,
      };

      return res.status(response.statusCode).json(response);
    });
});

export default router;
