import express from 'express';
import morgan from 'morgan';
import * as bodyParser from 'body-parser';
import mongoose from 'mongoose';
import config from 'config';

import logger from './logger';
import appRoutes from './routes/app';
import usersRoutes from './routes/users';
import booksRoutes from './routes/books';

const app = express();
const dbString = config.get('mongo.url');

mongoose.Promise = Promise;
mongoose.connect(dbString)
  .then(() => {
    if (process.env.NODE_ENV !== 'test') {
      logger.info('Successfully connected to database');
    }
  })
  .catch((err) => {
    logger.info(`Could not connect to database: ${err.message}`);
    process.exit(-1);
  });

if (process.env.NODE_ENV !== 'test') {
  app.use(morgan('combined', {
    stream: {
      write: msg => logger.info(msg),
    },
  }));
}

/* Middleware */
app.use(bodyParser.json());

/* Application routes */
app.use('/app', appRoutes);
app.use('/api/users', usersRoutes);
app.use('/api/books', booksRoutes);

/* Redirect to application on base route */
app.get('/', (req, res) => res.redirect('/app'));

export default app;
