import mongoose from 'mongoose';

const userSchema = new mongoose.Schema({
  email: {
    type: String,
    required: true,
    validate: {
      validator(v) {
        return /.+@.+\..+/.test(v);
      },
    },
  },
  password: {
    type: String,
    required: true,
  },
});

export default mongoose.model('user', userSchema);
