import mongoose from 'mongoose';
import symbolSchema from './symbol-schema';

const quoteSchema = new mongoose.Schema({
  text: {
    type: String,
    required: true,
  },
  description: {
    type: String,
    required: true,
  },
});

const bookSchema = new mongoose.Schema({
  owner: {
    type: String,
    required: false,
  },
  title: {
    type: String,
    required: true,
  },
  author: {
    type: String,
    default: 'unknown',
  },
  publicationInfo: {
    type: String,
    default: 'unknown',
  },
  ISBN: {
    type: Number,
    required: false,
  },
  genres: {
    type: [String],
    default: [],
  },
  themes: {
    type: [String],
    default: [],
  },
  symbols: {
    type: [symbolSchema],
    required: false,
    default: [],
  },
  summary: {
    type: String,
    default: '',
  },
  quotes: {
    type: [quoteSchema],
    required: false,
    default: [],
  },
});

export default mongoose.model('book', bookSchema);
