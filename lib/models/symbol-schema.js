import mongoose from 'mongoose';

const symbolSchema = new mongoose.Schema({
  name: {
    type: String,
    required: true,
  },
  description: {
    type: String,
    required: true,
  },
});

export default symbolSchema;
