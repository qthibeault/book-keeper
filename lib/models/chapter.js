import mongoose from 'mongoose';
import symbolSchema from './symbol-schema';

const chapterSchema = new mongoose.Schema({
  parent: {
    required: false,
    type: mongoose.Schema.Types.ObjectId,
  },
  title: {
    type: String,
    required: true,
    default: '',
  },
  summary: {
    type: String,
    required: false,
    default: '',
  },
  themes: {
    type: [String],
    required: false,
    default: [],
  },
  symbols: {
    type: [symbolSchema],
    required: false,
    default: [],
  },
});

export default mongoose.model('chapter', chapterSchema);
